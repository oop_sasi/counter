/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.counter;

/**
 *
 * @author admin
 */
public class Main {
    public static void main(String[] args) {
        Counter counter1 = new Counter();
        Counter counter2 = new Counter();
        Counter counter3 = new Counter();
        counter1.increase();
        counter1.increase();
        counter1.increase();
        counter1.print();
        counter2.print();
        counter2.increase();
        counter2.decrease();
        counter1.decrease();
        counter3.decrease();
        System.out.println("Print All Counter");
        counter1.print();
        counter2.print();
        counter3.print();
        
    }
}
